#include <iostream>
#include "array_utils.h"
#include "mpi_utils.h"

#define LENGTH      32000

int main(int argc, char *argv[]) {
  srand(static_cast<unsigned int>(time(nullptr)));

  mpi::environment env(argc, argv);
  mpi::communicator world;
  vector<int> arr;

  if (world.rank() == 0) {
    ArrayUtils::initRandom(arr, LENGTH);
  }
  MPI_Utils::sortParallel(world, arr);

  return 0;
}