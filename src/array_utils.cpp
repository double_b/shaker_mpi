#include "array_utils.h"


void ArrayUtils::initRandom(vector<int> &arr, int len) {
    for (int i = 0; i < len; i++) {
        arr.push_back(rand() % RAND_MAX_VAL);
    }
}

void ArrayUtils::print(vector<int> &arr) {
    cout << "[";
    for (int e : arr) {
        cout << e << " ";
    }
    cout << "]";

    cout << endl;
}

void ArrayUtils::shakerSort(vector<int> &arr, int begin, int end) {
    for (int i = begin; i <= end / 2; i++) { //todo <= check
        bool swapped = false;
        for (int j = i; j < end - i - 1; j++) { //one way
            if (arr[j] > arr[j + 1]) {
                int tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
                swapped = true;
            }
        }
        for (int j = (end - 2 - i); j > i; j--) { //and back
            if (arr[j] < arr[j - 1]) {
                int tmp = arr[j];
                arr[j] = arr[j - 1];
                arr[j - 1] = tmp;
                swapped = true;
            }
        }
        if (!swapped) break; //block (break if no element was swapped - the arr is sorted)
    }
}

unordered_map<string, int> ArrayUtils::getIndexes(vector<int> &arr, int procAmount, int rank) {

    if (arr.size() < procAmount) {
        procAmount = static_cast<int>(arr.size());
    }

    unordered_map<string, int> indexes;
    auto step = static_cast<int>(arr.size() / procAmount);

    int begin = rank * step;
    int end = begin + step - 1;

    if (rank == procAmount - 1) { // if last rank
        end += arr.size() % procAmount;
    }

    indexes["begin"] = begin;
    indexes["end"] = end;

    return indexes;
}

void ArrayUtils::printChunk(vector<int> &arr, int begin, int end) {
    cout << "[";
    for (int i = begin; i <= end; i++) {
        cout << arr[i] << " ";
    }
    cout << "]";

    cout << endl;
}

void ArrayUtils::shakerSort(vector<int> &arr) {
    int left = 0;
    auto right = static_cast<int>(arr.size() - 1);
    int flag = 1;

    while ((left < right) && flag > 0) {
        flag = 0;
        for (int i = left; i < right; i++) {
            if (arr[i] > arr[i + 1]) {
                int t = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = t;
                flag = 1;
            }
        }
        right--;
        for (int i = right; i > left; i--) {
            if (arr[i - 1] > arr[i]) {
                int t = arr[i];
                arr[i] = arr[i - 1];
                arr[i - 1] = t;
                flag = 1;
            }
        }
        left++;
    }
}