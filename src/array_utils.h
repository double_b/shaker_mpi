#ifndef SHAKER_MPI_ARRAY_UTILS_H
#define SHAKER_MPI_ARRAY_UTILS_H

#include <vector>
#include <cstdlib>
#include <iostream>
#include <unordered_map>

#define RAND_MAX_VAL    100

using namespace std;

class ArrayUtils {

public:

    static void initRandom(vector<int> &arr, int len);

    static void print(vector<int> &arr);

    static void printChunk(vector<int> &arr, int begin, int end);

    static void shakerSort(vector<int> &arr, int begin, int end);
    static void shakerSort(vector<int> &arr);

    static unordered_map<string, int> getIndexes(vector<int> &arr, int procAmount, int rank);

    static vector<int> shakerSortRet(vector<int> *arr, int begin, int end);
};


#endif //SHAKER_MPI_ARRAY_UTILS_H
