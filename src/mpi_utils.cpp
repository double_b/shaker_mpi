#include "mpi_utils.h"

void MPI_Utils::sortChunk(mpi::communicator world, int procAmount, int rank) {
  vector<int> arr;
  world.recv(0, 0, arr);

  unordered_map<string, int> indexes = ArrayUtils::getIndexes(arr, procAmount, rank);

  std::vector<int> sub(&arr[indexes["begin"]], &arr[indexes["end"] + 1]);

  ArrayUtils::shakerSort(sub);

  world.send(0, 0, sub);
}

void MPI_Utils::sortParallel(mpi::communicator &world, vector<int> &arr) {

  int p = world.size();
  mpi::timer timer;

  while (p != 1) {

    if (world.rank() == 0) {
      for (int i = 0; i < p; i++) {
        cout << "sending arr to Proc #" << i << endl;
        world.send(i, 0, arr);
      }
    }

    sortChunk(world, p, world.rank());

    if (world.rank() == 0) {
      vector<int> done;
      while (done.size() < arr.size()) {
        vector<int> msg;
        world.recv(mpi::any_source, mpi::any_tag, msg);
        done.insert(done.end(), msg.begin(), msg.end());
        cout << "done size:" << done.size() << endl;
      }
      arr = done;
    }
    p = p / 2;
  }

  if (world.rank() == 0) {
    ArrayUtils::shakerSort(arr);
    cout << endl;
    cout << "|======================|" << endl;
    cout << "  Processors amount: " << world.size() << endl;
    cout << "  Elapsed: " << timer.elapsed() << endl;
    cout << "|======================|" << endl;

  }

}
