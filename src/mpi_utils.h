#ifndef SHAKER_MPI_MPI_HELPER_H
#define SHAKER_MPI_MPI_HELPER_H

#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>
#include <boost/mpi.hpp>
#include <boost/mpi/timer.hpp>

#include "array_utils.h"

namespace mpi = boost::mpi;

class MPI_Utils {

 public:

  static void sortChunk(mpi::communicator world, int procAmount, int rank);
  static void sortParallel(mpi::communicator &world, vector<int> &arr);
};

#endif //SHAKER_MPI_MPI_HELPER_H
